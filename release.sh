#!/bin/sh
# # upload releases to github
#FILES="$(find 'builds/' -type f | tr '\n' ',')"
# %? removes the last character (in this case an unwanted , from the tr command replacing the last newline)
# get the most recent part of the changelog
VER="$(cat ver.txt)"
gh release create -R "${REPO}" "${VER}" -n "Update to ${VER}"
find builds/ -type f -exec gh release upload -R "${REPO}" --clobber "${VER}" {} \;