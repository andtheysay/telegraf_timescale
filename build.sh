#!/bin/sh

# TODO automate testing of the binaries? 

# compile for linux - pass in the GOARCH type as a parameter
compile_linux() {
    if [ -z "${1}" ]; then
        echo "error: GOARCH not defined"
        return
    fi
    env GOOS="linux" GOARCH="${1}" make -C telegraf \
    && tar czf "builds/telegraf-${VER}_linux_${1}.tar.gz" "telegraf/telegraf" \
    && rm telegraf/telegraf
}

# compile for windows - pass in the GOARCH type as a parameter
compile_windows() {
    if [ -z "${1}" ]; then
        echo "error: GOARCH not defined"
        return
    fi
    env GOOS="windows" GOARCH="${1}" make -C telegraf \
    && zip "builds/telegraf-${VER}_windows_${1}.zip" "telegraf/telegraf.exe" \
    && rm telegraf/telegraf.exe
}

# make the output dir
mkdir builds
# set version number
VER="$(cat ver.txt)"
# if linux
if [ "${1}" = "linux" ]; then
    compile_linux "386"
    compile_linux "amd64"
    compile_linux "arm64"
    compile_linux "arm"
fi

# if windows
if [ "${1}" = "windows" ]; then
    # install zip
    apt update -y && apt install --no-install-recommends -y zip
    compile_windows "386"
    compile_windows "amd64"
fi