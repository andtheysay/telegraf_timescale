#!/bin/sh

# exit if any commands fail
set -e

# get the version number of the latest stable release
VER="v$(gh release list -R influxdata/telegraf -L 1 | sed 's/[^0-9.L]*//g' | cut -d'L' -f1)"
# put the ver # into a file so the builder can read it in the next stage
printf "${VER}" > ver.txt
# clone the latest stable branch from influx's repo
git clone 'https://github.com/influxdata/telegraf' --depth 1 -b "${VER}"
cd telegraf
# fetch the timescaledb plugin from svenklemm's repo
git remote add postgresql "https://github.com/svenklemm/telegraf"
git fetch postgresql
git checkout postgresql/postgres -- plugins/outputs/postgresql